import socketio
import eventlet.wsgi
from flask import Flask, render_template

sio = socketio.Server()
app = Flask(__name__)


@app.route('/')
def index():
    """Serve the client-side application."""
    return render_template('websocket.html')


@sio.on('connect', namespace='/chat')
def connect(sid, message):
    print("connect ", sid)


@sio.on('request', namespace='/chat')
def message(sid, message):
    print('message', message['data'])
    sio.emit('response', {'data': message['data']}, room=sid, namespace='/chat')


@sio.on('disconnect', namespace='/chat')
def disconnect(sid):
    print('disconnect ', sid)


if __name__ == '__main__':
    # wrap Flask application with engineio's middleware
    app = socketio.Middleware(sio, app)

    # deploy as an eventlet WSGI server
    eventlet.wsgi.server(eventlet.listen(('127.0.0.1', 8000)), app)
