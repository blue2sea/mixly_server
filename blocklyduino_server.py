from flask import Flask, render_template
import os
import serial
import socketio
import eventlet.wsgi
import subprocess
import time
import serial.tools.list_ports

cur_path = os.path.dirname(os.path.realpath(__file__))
ino_path = cur_path + '/static/testArduino/testArduino.ino'
compile_cmd = cur_path + '/static/Arduino/arduino_debug --verify ' + cur_path + '/static/testArduino/testArduino.ino'
upload_cmd = cur_path + '/static/Arduino/arduino_debug --upload ' + cur_path + '/static/testArduino/testArduino.ino'

sio = socketio.Server()
app = Flask(__name__)

mixly_namespace = '/mixly_chat'
mixly_port = 7000


def get_arduino_port():
    ports = list(serial.tools.list_ports.comports())
    for p in ports:
        if 'CH340' in p[1] or 'Arduino' in p[1]:
            return p[0]


ard = serial.Serial(get_arduino_port(), 9600)
ard.close()


def write_ino_file(sketch):
    with open(ino_path, 'w') as f:
        f.write(sketch)


def read_from_arduino():
    if ard.is_open:
        return ard.readline()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/normal')
def show_normal():
    return render_template('index_simple.html')


@app.route('/advanced')
def show_advanced():
    return render_template('index.html')


@sio.on('connect', namespace=mixly_namespace)
def connect(sid, message):
    print('connect', sid)


@sio.on('compile', namespace=mixly_namespace)
def compiles(sid, message):
    run_arduino('compile', message['data'])


@sio.on('upload', namespace=mixly_namespace)
def upload(sid, message):
    if ard.is_open:
        print 'Close COM4 port when upload to Arduino'
        ard.close()
    run_arduino('upload', message['data'])


@sio.on('monitor', namespace=mixly_namespace)
def monitor(sid, message):
    if not ard.is_open:
        print 'Open COM4 port when start monitor'
        ard.open()
    sio.emit('monitor_response', {'data': read_from_arduino()}, room=sid, namespace=mixly_namespace)


@sio.on('stop', namespace=mixly_namespace)
def stops(sid, message):
    if ard.is_open:
        print 'Close COM4 port when stop monitor'
        ard.close()


@sio.on('disconnect', namespace=mixly_namespace)
def disconnect(sid):
    print('disconnect', sid)


def run_arduino(action, sketch):
    try:
        # update test.ino file by sketch
        write_ino_file(sketch)

        # get process to run sketch, log message
        proc = None
        if action == 'compile':
            subprocess.check_call(compile_cmd)
            proc = subprocess.Popen(compile_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        elif action == 'upload':
            subprocess.check_call(upload_cmd)
            proc = subprocess.Popen(upload_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

        (out, err) = proc.communicate()

        sio.emit('build_response', {'data': out + '\n\n' + action + ' Success!\n'}, namespace=mixly_namespace)
    except Exception as e:
        # build failed when exception occurs
        time.sleep(5)
        sio.emit('build_response', {'data': '\n\n' + str(e) + '\n\n' + action + ' Failed!'}, namespace=mixly_namespace)


if __name__ == '__main__':
    app = socketio.Middleware(sio, app)
    eventlet.wsgi.server(eventlet.listen(('127.0.0.1', mixly_port)), app)
